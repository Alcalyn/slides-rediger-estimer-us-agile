# Slides "Rediger et estimer des User Stories" Novembre 2018

[Support du cours sur l'agilité](https://alcalyn.gitlab.io/slides-rediger-estimer-us-agile/) à l'ESGI.

- [Version HTML](https://alcalyn.gitlab.io/slides-rediger-estimer-us-agile/)
- [Version LibreOffice](slides-rediger-estimer-us-agile.odp)
- [Version PDF](slides-rediger-estimer-us-agile.pdf)

![User story INVEST](html/img12.png)

![Planning poker](html/img20.png)


## Licence

Ces slides sont disponibles sous la licence [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).

![CC0 1.0 License](cc0.png)
